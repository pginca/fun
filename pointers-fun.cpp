#include <iostream>

typedef struct entity { int i; double d; } entity; 

int main() {

    const char* p_word = "abc";

    for(int i = 0; i < 3; i++, p_word++) {
        std::cout << &p_word << " " << *p_word << std::endl;
    }

    int x = 2;
    int* p_x = &x;
    *p_x = 4;
    std::cout << *p_x << std::endl;

    entity e;
    entity* p_e = &e;

    std::cout << &p_e << std::endl;

    p_e->d = 3.14;

    std::cout << p_e->d << std::endl;
    std::cout << (*p_e).d << std::endl;

    double sizes[] = {10.3, 13.4, 11.2, 19.4};
    double* p_sizes = sizes;

    std::cout << *(p_sizes + 2) << std::endl;
    std::cout << &sizes << std::endl;
    std::cout << sizes << std::endl;

    int* dyn_p = (int*)malloc(sizeof(int));
    *dyn_p = 10;

    std::cout << dyn_p << std::endl;
    std::cout << *dyn_p << std::endl;

    (*dyn_p) += 3;

    std::cout << *dyn_p << std::endl;

    free(dyn_p);

    int* cpp_p = new int(5); // allocates memory for an int the heap and assings it to 5

    std::cout << cpp_p << std::endl; // the address of the int pointed by cpp_p
    std::cout << &cpp_p << std::endl; // the address of cpp_p
    std::cout << &(*cpp_p) << std::endl; // the address ofthe int pointed by cpp_p
    std::cout << *cpp_p << std::endl; // the value of the int pointed by cpp_p

    delete cpp_p;

    cpp_p = new int[10]; // allocates memory for ten ints with unspecified initial values

    delete[] cpp_p;

    cpp_p = new int[10](); // allocates memory for ten ints with initial value 0.

    delete[] cpp_p;
    
    return 0;
}
