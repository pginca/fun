#include <iostream>
#include <memory>

struct Entity {

    Entity() {
	std::cout << "Entity Created!" << std::endl;
    }

    ~Entity() {
	std::cout << "Entity Destroyed!" << std::endl;
    }

    void print_entity() {
	std::cout << "Hello World" << std::endl;
    }
};

int main() {

    // Entity *e = new Entity();

    std::unique_ptr<Entity> e = std::make_unique<Entity>();

    e->print_entity();

    // delete e;
    
    return 0;
}
