#include <iostream>
#include <vector>
#include <algorithm>

#define LOG(X) std::cout << X << std::endl

namespace paulo {

    template<typename T>
    class Node
    {
    private:
        T key;
        Node<T>* next;

    public:
        Node(T key)
	    : key(key), next(nullptr) {}

	void setNext(Node<T>* next)
	{
	    this->next = next;
	}

	Node<T>* getNext()
	{
	    return this->next;
	}

	void setKey(T key)
	{
	    this->key = key;
	}

	T getKey()
	{
	    return this->key;
	}
    };
    
    template<typename T>
    class List
    {
    private:
        Node<T>* head;
	Node<T>* tail;

    public:
        List()
	    : head(nullptr), tail(head) {}

	void setHead(Node<T>* head)
	{
	    this->head = head;
	}

	Node<T>* getHead()
	{
	    return this->head;
	}

	void setTail(Node<T>* tail)
	{
	    this->tail = tail;
	}

	Node<T>* getTail()
	{
	    return this->tail;
	}
	
	int size() const
	{
	    int s = 0;
	    Node<T>* cur = this->head;

	    while(cur != nullptr)
	    {
		s++;
		cur = cur->getNext();
	    }

	    return s;
	}

	void insert(const T key)
	{
	    if(this->head == nullptr)
	    {
		this->head = new Node<T>(key);
		this->tail = this->head;
	    }
	    else
	    {
	        this->tail->setNext(new Node<T>(key));
	        this->tail = this->tail->getNext();
	    }
	}

	void erase(const T key)
	{
	    Node<T>* aux;
	    Node<T>* cur = this->head->getNext();
	    Node<T>* prev = this->head;

	    if(this->head->getKey() == key)
	    {
		aux = this->head;
		this->head = this->head->getNext();
		delete aux;
		return;
	    }
	    
	    while(cur != nullptr)
	    {
		if(cur->getKey() == key)
		{
		    if(cur == this->tail)
			this->tail = prev;

		    prev->setNext(cur->getNext());
		    aux = cur;
	            delete aux;
		    return;
		}
		prev = cur;
		cur = cur->getNext();
	    }
	}

	List<T>& operator+(List<T>& otherList)
	{
	    this->tail->setNext(otherList.head);
	    this->tail = otherList.tail;
	    return *this;
	}

	T operator[](int index)
	{
	    Node<T>* cur = this->head;

	    if(index >= this->size())
		return(-1);

	    for(int i = 0; i < index; i++)
		cur = cur->getNext();

	    return cur->getKey();
	}

	void print()
	{
	    for(int i = 0; i < this->size(); i++)
	    {
		std::cout << (*this)[i] << " ";
	    }
	    std::cout << std::endl;
	}

	~List()
	{
	    Node<T>* cur = this->head;
	    Node<T>* aux;

	    while(cur != nullptr)
	    {
		aux = cur;
		cur = cur->getNext();
		delete aux;
	    }
	}
    };
}

int main() {

    paulo::List<int> list1;
    paulo::List<int> list2;
    paulo::List<int> result;

    for(auto i : {4, 7, 12, 43, 34})
        list1.insert(i);

    for(auto i : {2, 56, 87, 45, 8})
        list2.insert(i);

    result = list1 + list2;

    result.print();

    result.erase(4);
    result.erase(7);
    result.erase(12);

    result.print();

    result.erase(2);

    result.print();

    result.erase(8);

    result.print();

    result.erase(43);
    result.erase(34);
    result.erase(56);
    result.erase(87);
    result.erase(45);

    LOG(result.size());
    result.print();

    return 0;
}
