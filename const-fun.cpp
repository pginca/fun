#include <iostream>

int get_size() { return 5; }

constexpr int size() { return 2; }

int main()
{
    int i = 0;

    /*
     * this pointer is a top-level const, meaning that
     * I cannot change its content. In other words
     * I cannot change the address it points to. It will
     * always point to i.
     */
    int* const p1 = &i;

    /*
     * This is also a top-level const because I cannot
     * change the value of the variable ci.
     */
    const int ci = 42;

    /*
     * This pointer is a low-level const. Meaning that
     * I cannot change the value of the variable it
     * points to. As consequence it needs to point to
     * a const variable. However it is possible to
     * make it point to a different address.
     */
    const int* p2 = &ci;

    const int ci2 = 3;

    /*
     * This is a const int pointer, meaning that it points
     * to a const int object. And it is also top-level const.
     * As a result, I cannot change neither its address or
     * the content of the address it points to.
     */
    const int* const p3 = &ci2;

    /* constant expressions are evalated at compile time */
    /* max_files is a constant expressions */
    const int max_files = 20;

    /* limit is a constant expression */
    const int limit = max_files + 1;

    /* staff_size is not constant expression */
    int staff_size = 27;

    /* sz is not a constant expression. It is evaluated
       a runtime */
    const int sz = get_size();

    constexpr int mf = 20;
    constexpr int limit2 = mf + 1;
    /* that is ok if the size is constexpr function*/
    constexpr int sz2 = size();
}
