#include <iostream>
#include <iterator>
#include <string>

int main()
{
    std::string v = "Hello World";

    std::cout << v << std::endl;

    std::string::iterator begin = v.begin();
    std::string::iterator end = v.end() - 1;

    for(; begin < end; ++begin, --end)
	std::swap(*begin, *end);

    std::cout << v << std::endl;

    std::cout << __FILE__ << " " << __LINE__ << std::endl;

}
