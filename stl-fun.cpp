#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>
#include <functional>

namespace paulo {

    template<typename itr, typename func>
    func for_each(itr begin, itr end, func f)
    {
	for(; begin != end; ++begin)
	    f(*begin);

	return f;
    }

    template<typename itr, typename type>
    type accumulate(itr begin, itr end, type acc)
    {
	for(; begin != end; ++begin)
	    acc += *begin;

	return acc;
    }

    
}

int main() {

    std::vector<int> values1 = {1, 2, 3, 4, 5};
    std::vector<double> values2 = {1.1, 2.2, 3.3, 4.4, 5.5};

    paulo::for_each(values1.begin(), values1.end(),
		    [](const int value)
		    {
		        std::cout << value << std::endl;
		    });

    int sum1 = paulo::accumulate(values1.begin(),
				 values1.end(), 0);
    std::cout << sum1 << std::endl;

    double sum2 = paulo::accumulate(values2.begin(),
				 values2.end(), 0.0);
    std::cout << sum2 << std::endl;
    
    return 0;


}
