#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>

int main() {
    std::vector<int> v;
    int sum = 0;

    for(int i = 0; i < 10; i++)
	v.push_back(i);

    std::for_each(v.begin(), v.end(),
		  [](int x)
		  {
		      std::cout << x << "\n";
		  });

    std::for_each(v.begin(), v.end(),
		  [&sum](int x)
		  {
		      sum += x;
		  });

    std::cout << "sum: " << sum << "\n";

    std::sort(v.begin(), v.end(),
	      [](int x, int y){
                  return x > y;
	      });

    std::cout << "\n\n";
    std::for_each(v.begin(), v.end(),
		  [](int x)
		  {
		      std::cout << x << "\n";
		  });

    sum = std::accumulate(v.begin(), v.end(), 0);
    std::cout << "sum: " << sum << "\n";

    return 0;
}
