
// CPP program to demonstrate  
#include <iostream> 
#include <cstdlib> 

void* operator new(size_t size, const char* file, int line) 
{
    void* p = malloc(size); 
    std::cout << size << " bytes allocated by " << file;
    std::cout << " in line " << line;
    std::cout << " in the address " << p << std::endl; 
    return p; 
} 
  
void operator delete(void* p) 
{ 
    std::cout << "memory from address " << p;
    std::cout << " deallocated" << std::endl; 
    free(p); 
} 
  
int main() 
{
    
    int* p = new(__FILE__, __LINE__) int; 
    
    delete p; 
} 

