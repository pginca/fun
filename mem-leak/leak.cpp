#include <cstdlib>
#include <iostream>
#include "leak.h"
#include "List.h"

List memMap;

class Memory
{
public:
    Memory() {}

    void display_mem_map()
    {
	memMap.print();
    }
    
    ~Memory()
    {
	this->display_mem_map();
    }
};

static Memory memory;
 
void* operator new(size_t size, const char* file, int line) 
{
    void* p = malloc(size); 
    memMap.insert({p, file, line});
    return p; 
} 
  
void operator delete(void* p) 
{ 
    memMap.erase(p);
    free(p);
}
