#include <iostream>
#include "List.h"

Node::Node(MemMapItem key)
    : key(key), next(nullptr) {}

void Node::setNext(Node* next)
{
    this->next = next;
}

Node* Node::getNext()
{
    return this->next;
}

void Node::setKey(MemMapItem key)
{
    this->key = key;
}

MemMapItem Node::getKey()
{
    return this->key;
}
    
List::List()
    : head(nullptr), tail(head) {}

void List::setHead(Node* head)
{
    this->head = head;
}

Node* List::getHead()
{
    return this->head;
}

void List::setTail(Node* tail)
{
    this->tail = tail;
}

Node* List::getTail()
{
    return this->tail;
}
	
int List::size() const
{
    int s = 0;
    Node* cur = this->head;

    while(cur != nullptr)
    {
	s++;
	cur = cur->getNext();
    }

    return s;
}

void List::insert(const MemMapItem key)
{
    if(this->head == nullptr)
    {
	this->head = (Node *)malloc(sizeof(Node));
	this->head->setKey(key);
	this->head->setNext(nullptr);
	this->tail = this->head;
    }
    else
    {
	this->tail->setNext((Node *)malloc(sizeof(Node)));
	this->tail->getNext()->setKey(key);
	this->tail->getNext()->setNext(nullptr);
	this->tail = this->tail->getNext();
    }
}

void List::erase(void* ptr)
{
    Node* aux;
    Node* cur = this->head->getNext();
    Node* prev = this->head;

    if(this->head->getKey().address == ptr)
    {
	aux = this->head;
	this->head = this->head->getNext();
	free(aux);
	return;
    }
	    
    while(cur != nullptr)
    {
	if(cur->getKey().address == ptr)
	{
	    if(cur == this->tail)
		this->tail = prev;

	    prev->setNext(cur->getNext());
	    aux = cur;
	    free(aux);
	    return;
	}
	prev = cur;
	cur = cur->getNext();
    }
}

void List::print()
{
    Node* cur = this->getHead();
    while(cur != nullptr)
    {
	std::cout << "Leaked object at ";
	std::cout << cur->getKey().address;
	std::cout << ". Allocated in ";
	std::cout << cur->getKey().file;
	std::cout << ":" << cur->getKey().line;
	std::cout << std::endl;

	cur = cur->getNext();
    }
}

List::~List()
{
    Node* cur = this->head;
    Node* aux;

    while(cur != nullptr)
    {
	aux = cur;
	cur = cur->getNext();
	free(aux);
    }
}
