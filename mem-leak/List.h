#pragma once

struct MemMapItem
{
    void* address;
    const char* file;
    int line;
};

class Node
{
private:
    MemMapItem key;
    Node* next;

public:
    Node(MemMapItem key);
    void setNext(Node* next);
    Node* getNext();
    void setKey(MemMapItem key);
    MemMapItem getKey();
};

class List
{
private:
    Node* head;
    Node* tail;

public:
    List();
    void setHead(Node* head);
    Node* getHead();
    void setTail(Node* tail);
    Node* getTail();
    int size() const;
    void insert(const MemMapItem key);
    void erase(void* ptr);
    void print();
    ~List();
};
