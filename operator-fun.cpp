#include <iostream>

class Pair
{
public:
    int x, y;

    Pair(int x, int y)
	: x(x), y(y) {}

    Pair Add(const Pair& other) const
    {
	return *this + other;
    }

    Pair operator+(const Pair& other) const
    {
	return Pair(this->x + other.x, this->y + other.y);
    }

    void Print()
    {
	std::cout << this->x << ", " << this->y << std::endl;
    }

    bool operator==(const Pair& other) const
    {
	return (this->x == other.x) && (this->y == other.y);
    }

    bool operator!=(const Pair& other) const
    {
	return !(*this == other);
    }

    ~Pair() {}
};

std::ostream& operator<<(std::ostream& stream, const Pair& other)
{
    stream << other.x << ", " << other.y;
    return stream;
}

int main()
{
    Pair p1(1, 2);
    Pair p2(2, 3);

    Pair result1 = p1.Add(p2);
    std::cout << p1 << std::endl;

    Pair result2 = p1 + p2;
    std::cout << p2 << std::endl;

    if(p1 == p2)
	std::cout << "equal" << std::endl;
    else
	std::cout << "not equal" << std::endl;

    if(p1 != p2)
	std::cout << "not equal" << std::endl;
    else
	std::cout << "equal" << std::endl;
	
}
