#include <iostream>
#include <cstring>

using namespace std;

int isSafe(int posM, int row, int col,
	   int n, int m, int posVisited) {

  return(row >= 0 && col >= 0 &&
	 row < n && col < m &&
	 posM && !posVisited);

}

/* This function checks if the neighbours of a given
 * position form an island in depth first search
 * manner. It return the size of the island. */
int dfs(int M[][5], int row, int col,
	int n, int m, int visited[][5]) {

  int rowNbr[] = {-1, -1, -1,  0, 0,  1, 1, 1};
  int colNbr[] = {-1,  0,  1, -1, 1, -1, 0, 1};
  int islandSize = 1;

  visited[row][col] = 1;

  for(int k = 0; k < 8; k++) {
    int newRow = row + rowNbr[k];
    int newCol = col + colNbr[k];
    if(isSafe(M[newRow][newCol], newRow, newCol,
	      n, m, visited[newRow][newCol]))
      islandSize += dfs(M, newRow, newCol, n, m, visited);
  }

  return islandSize;

}

/* This function compute the number of islands in a
 * given matrix. An island is a sequence of ones. 
   It returns the number of islands. */
int countNumOfIslands(int M[][5], int n, int m) {

  int numOfIslands = 0;
  int visited[5][5];

  memset(visited, 0, sizeof(visited));

  for(int row = 0; row < n; row++)
    for(int col = 0; col < m; col++)
      if(M[row][col] && !visited[row][col]) {
        dfs(M, row, col, n, m, visited);
	numOfIslands++;
      }
  
  return numOfIslands;
  
}

int main() {

  int n, m;

  int M[][5] = {{0, 0, 0, 0, 1},
		{0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0},
                {1, 0, 0, 0, 0}};

  m = sizeof(M) / sizeof(M[0]);
  n = sizeof(M[0]) / sizeof(M[0][0]);

  cout << countNumOfIslands(M, n, m) << endl;

  return 0;

}
