#include <iostream>
#include <unordered_map>

using namespace std;

/* Given an array checks if any two 
 * elements add up to sum */
int hasSum(int v[], int n, int sum) {

  unordered_map<int, int> umap;
  unordered_map<int, int>::iterator it;

  for(int i = 0; i < n; i++)
    umap[v[i]]++;

  for(int i = 0; i < n; i++) {
    umap[v[i]]--;
    it = umap.find(sum - v[i]);
    if(it != umap.end() && it->second)
      return 0;
  }

  return 1;
}

int main() {

  int v[] = {5, 5, 5, 5, 5};
  int n;
  int sum = 5;

  n = sizeof(v) / sizeof(v[0]);

  if(hasSum(v, n, sum))
    cout << "There are two elements that add up to sum" << endl;
  
  return 0;
}
